#include "ofApp.h"

/*long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}*/

ofApp * oa;

uint8_t ofApp::sizecvt(const int read)
{
  /* digitalRead() and friends from wiringpi are defined as returning a value
  < 256. However, they are returned as int() types. This is a safety function */

  if (read > 255 || read < 0)
  {
    printf("Invalid data from wiringPi library\n");
    //exit(EXIT_FAILURE);
  }
  return (uint8_t)read;
}


void terminate(int sig) {
	cout << "terminate" << endl;
	oa->soundPlayer.stop();
	oa->soundPlayer.unloadSound();
	serialPutchar (oa->serial, 0);
	digitalWrite(HIGHLOYLYLED,0);
	digitalWrite(LOWLOYLYLED,0);
	ofExit();
}

//--------------------------------------------------------------
void ofApp::setup(){

	signal(SIGINT, terminate);
	signal(SIGTERM, terminate);

	wiringPiSetup();
	//softPwmCreate (1, 10, 200) ;
	pinMode(HIGHLOYLYLED, OUTPUT);
	pinMode(LOWLOYLYLED, OUTPUT);
	pinMode(SAUNAJENKKAMODEPIN, INPUT);
	pullUpDnControl (SAUNAJENKKAMODEPIN, PUD_UP);
	pinMode(HIGHLOYLYCALIBRATEIPIN, INPUT);
	pullUpDnControl (HIGHLOYLYCALIBRATEIPIN, PUD_UP);

	if ((serial = serialOpen ("/dev/ttyAMA0", 9600)) < 0) {
		cout << "Sarjaporttidilemma." << endl;
  }

	for (int i = 0; i > 240; i++) {
		serialPutchar (serial, i);
	}
	serialPutchar(serial,255);

	lastDhtRead = ofGetElapsedTimeMillis();

	directory.open("/media/usb/");
	//directory.allowExt("mp3");
	directory.allowExt("wav");
	directory.listDir();
	for(int i = 0; i < directory.numFiles(); i++) {
		ofLogNotice(directory.getPath(i));
	}
	directoryIterator = 1;

	lowTreshold = 0.1;
	highTreshold = 0.8;
	maxHumidity = 0;
	minHumidity = 100;

	currentHumidity = 0;
	targetHumidity = 0;
	currentLoyly = 0.2;

	maxHumidityCandidate = 0;
	minHumidityCandidate = 100;

	loylyCalibrationCounter = 0;

	lastPwm = 0;

	oa = this;
	saunajenkkaMode = true;	
	serialPutchar (serial, 0);

	ofSetFrameRate(30);

	loylyUpdateStep = 0.001;

	cout << ofGetFrameRate() << endl;
}

//-------------------------------------------------------------
void ofApp::update() {

	if (!soundPlayer.getIsPlaying()) {
		if (saunajenkkaMode) {
			if (soundPlayer.isLoaded()) soundPlayer.unloadSound();
			while (!soundPlayer.loadSound("/ro/home/pi/sj.wav"));
			soundPlayer.setLoop(true);
			soundPlayer.play();	
		} else {
			if (soundPlayer.isLoaded()) soundPlayer.unloadSound();
			directory.open("/media/usb/.saunajenkka");
			if (directory.exists()) {
				directory.open("/media/usb/");
				directory.allowExt("wav");
				directory.listDir();
				soundPlayer.setLoop(false);
				if (directoryIterator > directory.numFiles()) directoryIterator = 0;
				while (!soundPlayer.loadSound(directory.getPath(directoryIterator),true));
				soundPlayer.play();
				directoryIterator++;
			} else {
				while (!soundPlayer.loadSound("/ro/home/pi/fw.wav"));
				soundPlayer.play();
				soundPlayer.setLoop(true);
			}
			
		}
	}

	
	if (serialDataAvail(serial)) {
		int r = (int)serialGetchar(serial);
		if (r > 0 && r < 100) {
			targetHumidity = r;
			if (maxHumidity < r) {
				maxHumidity = r;
			}
			if (minHumidity > r) {
				minHumidity = r;
			}

			if (minHumidityCandidate > r) minHumidityCandidate = r;
			if (maxHumidityCandidate < r) maxHumidityCandidate = r;
		
			loylyCalibrationCounter++;
			if (loylyCalibrationCounter > LOYLYCALIBRATECOUNTTO) {
				
				maxHumidity = maxHumidityCandidate;
				minHumidity = minHumidityCandidate;

				maxHumidityCandidate = 0;
				minHumidityCandidate = 100;

				loylyCalibrationCounter = 0;
			}

			if (maxHumidity < minHumidity + 5) targetLoyly = 0;
			else targetLoyly = ((r - minHumidity) * 1.0 ) / (maxHumidity - minHumidity);

			//serialPutchar(serial,(int)(currentLoyly * 240));

			loylyUpdateStep = abs(targetLoyly - currentLoyly) / (ofGetTargetFrameRate()*2.5);

			cout << ofGetFrameRate() << " MIN " << minHumidity << " CURRENT " << targetHumidity
						<< " MAX " << maxHumidity << " COUNT " << loylyCalibrationCounter
						<< " MINC " << minHumidityCandidate << " MAXC "
						<< maxHumidityCandidate
						<< " TLOYLY " << targetLoyly << " CLOYLY " << currentLoyly << " " << loylyUpdateStep << endl;

			

		} else {
			cout << "ERROR: " << (r - 100)*-1 << endl;
			serialPutchar(serial,253);		// this will reset DHT22 sensor
		}
	}


	//if (currentLoyly < targetLoyly) currentLoyly += 0.003;
	//else if (currentLoyly > targetLoyly) currentLoyly -= 0.001;
	if (currentLoyly < targetLoyly) {
		currentLoyly += loylyUpdateStep;
		if (currentLoyly > targetLoyly) currentLoyly = targetLoyly;
	}
	else if (currentLoyly > targetLoyly) {
		currentLoyly -= loylyUpdateStep;
		if (currentLoyly < targetLoyly) currentLoyly = targetLoyly;
	}

	if (maxHumidity == 0) return;	// no value from sensor yet 

	float newSpeed = -1;
	if (currentLoyly < lowTreshold) newSpeed = LOWESTPLAYBACKSPEED;
	else if (currentLoyly > highTreshold) newSpeed = 1;
	else newSpeed = ((currentLoyly - lowTreshold) * (1 - LOWESTPLAYBACKSPEED) / (highTreshold - lowTreshold) + LOWESTPLAYBACKSPEED);

	//cout << "NS " << newSpeed << endl;

	soundPlayer.setSpeed(newSpeed);

	//cout << "CL " << currentLoyly << endl;

	digitalWrite(HIGHLOYLYLED, (currentLoyly > highTreshold));
	digitalWrite(LOWLOYLYLED, (currentLoyly < lowTreshold));

	int newPwm = (int)(currentLoyly * 240);
	if (lastPwm!=newPwm){
		serialPutchar(serial,(int)(currentLoyly * 240));
		lastPwm = newPwm;
		//cout << "!!!" << lastPwm  << endl;
	}
	
	if (digitalRead(SAUNAJENKKAMODEPIN) != saunajenkkaMode) {
		saunajenkkaMode = !saunajenkkaMode;
		soundPlayer.stop();
	}	

	if (!digitalRead(HIGHLOYLYCALIBRATEIPIN)) {
		highTreshold = currentLoyly - 0.01;
		cout << "calibrate! " << highTreshold << endl;
	}

}

//--------------------------------------------------------------
void ofApp::draw(){

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	cout << "NEXT" << endl;
	soundPlayer.stop();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
