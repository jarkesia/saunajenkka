#pragma once

#include "ofMain.h"
#include "wiringPi.h"
#include <wiringSerial.h>
#include "softPwm.h"
#include <signal.h>

#define MAXTIMINGS 85
#define DHTPIN 7  
#define LOWESTPLAYBACKSPEED 0.2

#define MAXHUMIDITY 100

#define HIGHLOYLYLED 12
#define LOWLOYLYLED 13
#define HIGHLOYLYCALIBRATEIPIN 3
#define SAUNAJENKKAMODEPIN 14


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		int readDht();
		uint8_t sizecvt(const int read);
		long lastDhtRead;

		float lowTreshold;
		float highTreshold;	
		int targetHumidity;
		float currentHumidity;
		int maxHumidity;
		int minHumidity;
		int loylyCalibrationCounter;
		int minHumidityCandidate;
		int maxHumidityCandidate;
		float currentLoyly;
		float targetLoyly;
		float loylyUpdateStep;
		#define LOYLYCALIBRATECOUNTTO 300

		int lastPwm;		
		int serial;

		bool saunajenkkaMode;
		
		//ofSoundPlayer test;
		ofSoundPlayer soundPlayer;
		ofOpenALSoundPlayer ooa;
		ofDirectory directory;
		int directoryIterator;

};
